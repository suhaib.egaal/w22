import React from 'react';
import Component from './Component';

const App = () => {
  return (
    <div>
      <h1>W22 country example</h1>
      <Component country="Finland" />
      <Component country="Norway" />
    </div>
  );
};

export default App;
