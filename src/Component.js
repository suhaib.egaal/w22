import React from 'react';

const Component = (props) => {
  return (
    <div>
      <p>Hello {props.country}!</p>
    </div>
  );
};

export default Component;
